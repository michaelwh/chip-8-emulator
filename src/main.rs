use std::{io, fmt::Debug};
use macroquad::prelude::*;
use tui::{backend::CrosstermBackend, Terminal};
use chip_8_emulator::{CPU, draw_screen};


// Instruction set http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#3.1

#[macroquad::main("CHIP8 Emulator")]
async fn main() {


    let  mut state = CPU::new();
    let program = std::fs::read("Stars [Sergey Naydenov, 2010].ch8").expect("Error reading file");
    state.memory.load_program(&program);

    loop {
        if state.delay_timer > 0 {
            state.delay_timer -= 1;
        }

        if state.sound_timer > 0 {
            state.sound_timer -= 1;
        }

        for _ in 0..50 {
        // if is_key_pressed(KeyCode::N) {
            // println!("Tick");
            state.fetch_and_run();
            // println!("Registers {:?}", state.registers);
            // println!("Stack {:?}", state.stack);
            // println!("Stack pointer {:?}", state.stack_pointer);
            // println!("Address register {:?}", state.address_register);
            // println!("Program counter {:?} {:#x}", state.program_counter, state.program_counter);
            
        // }
        }

        clear_background(BLACK);
        draw_screen(&state.screen);
        next_frame().await
    }
}
