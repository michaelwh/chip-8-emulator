// Instruction set http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#3.1

use rand::{RngCore, SeedableRng};
use rand_xorshift::XorShiftRng;
use macroquad::prelude::{screen_width, screen_height, BLACK, GREEN, WHITE, draw_rectangle};

pub const SCREEN_WIDTH: usize = 128;
pub const SCREEN_HEIGHT: usize = 64;

#[derive(Debug)]
pub struct CPU {
    pub memory: Memory,
    pub registers: [u8; 16],   // V
    pub address_register: u16, // I
    pub program_counter: u16,  // pc
    pub stack: [u16; 16],
    pub stack_pointer: u16,       // sp
    pub screen: [[bool; SCREEN_WIDTH]; SCREEN_HEIGHT], // gfx row, col i.e. y, x
    pub delay_timer: u8,
    pub sound_timer: u8,
    pub keys: [bool; 16],
    rng: XorShiftRng,
}

impl CPU {
    pub fn new() -> CPU {
        CPU {
            memory: Memory::new(),
            registers: [0; 16],
            address_register: 0,
            program_counter: 0x200,
            stack: [0; 16],
            stack_pointer: 0,
            screen: [[false; SCREEN_WIDTH]; SCREEN_HEIGHT], // row, col i.e. y, x
            delay_timer: 0,
            sound_timer: 0,
            keys: [false; 16],
            rng: XorShiftRng::seed_from_u64(0),
        }
    }

    pub fn fetch_and_run(&mut self) {
        let instruction = self.memory.get_instruction(self.program_counter);
        // println!("Instruction: {:#x}", instruction);
        match instruction & 0xF000 {
            0x0000 => match instruction {
                0x00E0 => self.clear_screen_instruction(),
                0x00EE => self.return_from_subroutine_instruction(),
                _ => panic!("Instruction '{}' not implemented", instruction),
            },
            0x1000 => self.jump_instruction(instruction),
            0x2000 => self.call_subroutine(instruction),
            0x3000 => self.skip_if_reg_equal_to_constant(instruction),
            0x4000 => self.skip_if_reg_not_equal_to_constant(instruction),
            0x5000 => self.skip_if_reg_equals_reg(instruction),
            0x6000 => self.set_reg_to_constant(instruction),
            0x7000 => self.add_constant_to_reg(instruction),
            0x8000 => match instruction & 0x000F {
                0x0000 => self.set_reg_to_reg(instruction),
                0x0001 => self.set_reg_to_reg_or_reg(instruction),
                0x0002 => self.set_reg_to_reg_and_reg(instruction),
                0x0003 => self.set_reg_to_reg_xor_reg(instruction),
                0x0004 => self.set_reg_to_reg_plus_reg(instruction),
                0x0005 => self.set_reg_to_reg_sub_reg(instruction),
                0x0006 => self.set_reg_to_reg_shr_1(instruction),
                0x0007 => self.set_reg_to_reg_subn_reg(instruction),
                0x000e => self.set_reg_to_reg_shl_1(instruction),
                _ => panic!("Instruction '{}' not implemented", instruction),
            },
            0x9000 => self.skip_next_instruction_if_reg_not_equal_to_reg(instruction),
            0xa000 => self.set_address_register_to_constant(instruction),
            0xb000 => self.jump_to_location_plus_reg_0(instruction),
            0xc000 => self.set_reg_to_random_and_constant(instruction),
            0xd000 => self.display_sprite(instruction),
            0xe000 => match instruction & 0x00FF {
                0x009e => self.skip_if_key_pressed(instruction),
                0x00a1 => self.skip_if_key_not_pressed(instruction),
                _ => panic!("Instruction '{}' not implemented", instruction),
            },
            0xf000 => match instruction & 0x00FF {
                0x0007 => self.set_reg_to_delay_timer_value(instruction),
                0x000a => self.wait_for_key_press_and_store_in_reg(instruction),
                0x0015 => self.set_delay_timer_to_reg(instruction),
                0x0018 => self.set_sound_timer_to_reg(instruction),
                0x001e => self.add_address_reg_and_reg_storing_in_address_reg(instruction),
                0x0029 => self.get_sprite_for_digit_in_reg(instruction),
                0x0033 => self.store_bcd_representation_of_reg_in_memory(instruction),
                0x0055 => self.store_regs_v0_to_vx_in_memory(instruction),
                0x0065 => self.read_memory_into_regs_v0_to_vx(instruction),
                _ => panic!("Instruction '{}' not implemented", instruction),
            },
            _ => panic!("Instruction '{}' not implemented", instruction),
        }
    }

    // 00E0 CLS
    fn clear_screen_instruction(&mut self) {
        for row in self.screen.iter_mut() {
            for val in row.iter_mut() {
                *val = false;
            }
        }
        self.program_counter += 2;
    }

    // 00EE RET
    fn return_from_subroutine_instruction(&mut self) {
        self.stack_pointer -= 1;
        self.program_counter = self.stack[self.stack_pointer as usize];
    }

    // 1nnn JP
    fn jump_instruction(&mut self, instruction: u16) {
        // println!("Jump from {:#x}", self.program_counter);
        self.program_counter = instruction & 0x0FFF;
        // println!("Jump to {:#x}", self.program_counter);
    }

    // 2nnn CALL
    fn call_subroutine(&mut self, instruction: u16) {
        self.stack[self.stack_pointer as usize] = self.program_counter;
        self.stack_pointer += 1;
        self.program_counter = instruction & 0x0FFF;
    }

    // 3xkk SE Vx, byte
    fn skip_if_reg_equal_to_constant(&mut self, instruction: u16) {
        if self.registers[((instruction & 0x0F00) >> 8) as usize] == (instruction & 0x00FF) as u8 {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    // 4xkk SNE Vx, byte
    fn skip_if_reg_not_equal_to_constant(&mut self, instruction: u16) {
        if self.registers[instruction.get_axaa() as usize] != (instruction & 0x00FF) as u8 {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    // 5xy0 SE Vx Vy
    fn skip_if_reg_equals_reg(&mut self, instruction: u16) {
        if self.registers[instruction.get_axaa() as usize]
            == self.registers[instruction.get_aaya() as usize]
        {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    // 6xkk LD Vx, byte
    fn set_reg_to_constant(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = (instruction & 0x00FF) as u8;
        self.program_counter += 2;
    }

    // 7xkk ADD Vx, byte
    fn add_constant_to_reg(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = self.registers[instruction.get_axaa() as usize].wrapping_add((instruction & 0x00FF) as u8);
        self.program_counter += 2;
    }

    // 8xy0 - LD Vx, Vy
    fn set_reg_to_reg(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] =
            self.registers[instruction.get_aaya() as usize];
        self.program_counter += 2;
    }

    // 8xy1 - OR Vx, Vy  Set Vx = Vx OR Vy.
    fn set_reg_to_reg_or_reg(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = self.registers
            [instruction.get_axaa() as usize]
            | self.registers[instruction.get_aaya() as usize];
        self.program_counter += 2;
    }

    // 8xy2 - AND Vx, Vy  Set Vx = Vx AND Vy.
    fn set_reg_to_reg_and_reg(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = self.registers
            [instruction.get_axaa() as usize]
            & self.registers[instruction.get_aaya() as usize];
        self.program_counter += 2;
    }

    // 8xy3 - XOR Vx, Vy  Set Vx = Vx XOR Vy.
    fn set_reg_to_reg_xor_reg(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = self.registers
            [instruction.get_axaa() as usize]
            ^ self.registers[instruction.get_aaya() as usize];
        self.program_counter += 2;
    }

    // 8xy4 - ADD Vx, Vy  Set Vx = Vx + Vy, set VF = carry.
    fn set_reg_to_reg_plus_reg(&mut self, instruction: u16) {
        let (result, carry) = self.registers[instruction.get_axaa() as usize]
            .overflowing_add(self.registers[instruction.get_aaya() as usize]);
        self.registers[instruction.get_axaa() as usize] = result;
        self.registers[0xf] = if carry { 1 } else { 0 };
        self.program_counter += 2;
    }

    // 8xy5 - SUB Vx, Vy  Set Vx = Vx - Vy, set VF = NOT borrow.
    fn set_reg_to_reg_sub_reg(&mut self, instruction: u16) {
        let (result, overflow) = self.registers[instruction.get_axaa() as usize]
            .overflowing_sub(self.registers[instruction.get_aaya() as usize]);
        self.registers[instruction.get_axaa() as usize] = result;
        self.registers[0xf] = if overflow { 0 } else { 1 };
        self.program_counter += 2;
    }

    // 8xy6 - SHR Vx {, Vy}  Set Vx = Vx SHR 1
    fn set_reg_to_reg_shr_1(&mut self, instruction: u16) {
        self.registers[0xf] = self.registers[instruction.get_axaa() as usize] & 0x01; // if lsb is 1
        self.registers[instruction.get_axaa() as usize] =
            self.registers[instruction.get_axaa() as usize] >> 1;
        self.program_counter += 2;
    }

    // 8xy7 - SUBN Vx, Vy  Set Vx = Vy - Vx, set VF = NOT borrow.
    fn set_reg_to_reg_subn_reg(&mut self, instruction: u16) {
        let (result, overflow) = self.registers[instruction.get_aaya() as usize]
            .overflowing_sub(self.registers[instruction.get_axaa() as usize]);
        self.registers[instruction.get_axaa() as usize] = result;
        self.registers[0xf] = if overflow { 0 } else { 1 };
        self.program_counter += 2;
    }

    // 8xyE - SHL Vx {, Vy}  Set Vx = Vx SHL 1
    fn set_reg_to_reg_shl_1(&mut self, instruction: u16) {
        self.registers[0xf] = (self.registers[instruction.get_axaa() as usize] & 0x80) >> 7; // if msb is 1
        self.registers[instruction.get_axaa() as usize] =
            self.registers[instruction.get_axaa() as usize] << 1;
        self.program_counter += 2;
    }

    //  9xy0 - SNE Vx, Vy
    fn skip_next_instruction_if_reg_not_equal_to_reg(&mut self, instruction: u16) {
        if self.registers[instruction.get_axaa() as usize]
            != self.registers[instruction.get_aaya() as usize]
        {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    // Annn - LD I, addr
    fn set_address_register_to_constant(&mut self, instruction: u16) {
        self.address_register = instruction & 0x0FFF;
        self.program_counter += 2;
    }

    // Bnnn - JP V0, addr
    fn jump_to_location_plus_reg_0(&mut self, instruction: u16) {
        self.program_counter = (instruction & 0x0FFF) + (self.registers[0] as u16);
    }

    // Cxkk - RND Vx, byte  Set Vx = random byte AND kk.
    fn set_reg_to_random_and_constant(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] =
            (self.rng.next_u32() as u8) & ((instruction & 0x00FF) as u8);
        self.program_counter += 2;
    }

    // Dxyn - DRW Vx, Vy, nibble
    fn display_sprite(&mut self, instruction: u16) {
        let x_start = self.registers[instruction.get_axaa() as usize] as usize % SCREEN_WIDTH;
        let y_start = self.registers[instruction.get_aaya() as usize] as usize % SCREEN_HEIGHT;
        // println!("Draw: {:#x}, x {}, y {}", instruction, x_start, y_start);
        let n = (instruction & 0x000F) as usize;
        self.registers[0xf] = 0;
        // remembering our screen array is row, col i.e. y, x indexed
        for i in 0..n {
            let y = y_start + i;
            let byte: u8 = self.memory.memory[(self.address_register as usize) + i];
            for j in 0..8 {
                // sprites are always 8 wide
                let x = j + x_start;
                let bit: bool = byte & (1 << (7 - j)) != 0;
                if self.screen[y][x] && bit {
                    self.registers[0xf] = 1;
                }
                self.screen[y][x] ^= bit;
            }
        }
        self.program_counter += 2;
    }

    //  Ex9E - SKP Vx
    fn skip_if_key_pressed(&mut self, instruction: u16) {
        if self.keys[self.registers[instruction.get_axaa() as usize] as usize] {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    //  ExA1 - SKNP Vx
    fn skip_if_key_not_pressed(&mut self, instruction: u16) {
        if !self.keys[self.registers[instruction.get_axaa() as usize] as usize] {
            self.program_counter += 2;
        }
        self.program_counter += 2;
    }

    //  Fx07 - LD Vx, DT
    fn set_reg_to_delay_timer_value(&mut self, instruction: u16) {
        self.registers[instruction.get_axaa() as usize] = self.delay_timer;
        self.program_counter += 2;
    }

    //  Fx0A - LD Vx, K
    fn wait_for_key_press_and_store_in_reg(&mut self, instruction: u16) {
        // Note that when multiple keys are pressed will place the first in they keys
        // array in the register
        if let Some((key_i, _)) = self.keys.iter().enumerate().find(|&(_, &val)| val) {
            self.registers[instruction.get_axaa() as usize] = key_i as u8;
            self.program_counter += 2;
        };
    }

    //  Fx15 - LD DT, Vx
    fn set_delay_timer_to_reg(&mut self, instruction: u16) {
        self.delay_timer = self.registers[instruction.get_axaa() as usize];
        self.program_counter += 2;
    }

    //  Fx18 - LD ST, Vx
    fn set_sound_timer_to_reg(&mut self, instruction: u16) {
        self.sound_timer = self.registers[instruction.get_axaa() as usize];
        self.program_counter += 2;
    }

    //  Fx1E - ADD I, Vx
    //  Set I = I + Vx.
    fn add_address_reg_and_reg_storing_in_address_reg(&mut self, instruction: u16) {
        self.address_register += self.registers[instruction.get_axaa() as usize] as u16;
        self.program_counter += 2;
    }

    //  Fx29 - LD F, Vx
    //  Set I = location of sprite for digit Vx.
    fn get_sprite_for_digit_in_reg(&mut self, instruction: u16) {
        self.address_register = self.registers[instruction.get_axaa() as usize] as u16 * 5;
        self.program_counter += 2;
    }

    // Fx33 - LD B, Vx
    // Store BCD representation of Vx in memory locations I, I+1, and I+2.
    fn store_bcd_representation_of_reg_in_memory(&mut self, instruction: u16) {
        let val = self.registers[instruction.get_axaa() as usize];
        self.memory.memory[self.address_register as usize] = val / 100;
        self.memory.memory[self.address_register as usize + 1] = (val / 10) % 10;
        self.memory.memory[self.address_register as usize + 2] = val % 10;
        self.program_counter += 2;
    }

    // Fx55 - LD [I], Vx
    // Store registers V0 through Vx in memory starting at location I.
    fn store_regs_v0_to_vx_in_memory(&mut self, instruction: u16) {
        let x = instruction.get_axaa() as usize;
        self.memory.put_slice(self.address_register as usize, &self.registers[0..=x]);
        self.program_counter += 2;
    }

    // Fx65 - LD Vx, [I]
    // Read registers V0 through Vx from memory starting at location I.
    fn read_memory_into_regs_v0_to_vx(&mut self, instruction: u16) {
        let x = instruction.get_axaa() as usize;
        for i in 0..=x {
            self.registers[i] = self.memory.memory[self.address_register as usize + i];
        }
        self.program_counter += 2;
    }

}

trait Instruction {
    fn get_axaa(&self) -> u8;
    fn get_aaya(&self) -> u8;
    fn get_axya(&self) -> (u8, u8);
}

impl Instruction for u16 {
    fn get_axaa(&self) -> u8 {
        ((self & 0x0F00) >> 8) as u8
    }

    fn get_aaya(&self) -> u8 {
        ((self & 0x00F0) >> 4) as u8
    }

    fn get_axya(&self) -> (u8, u8) {
        (self.get_axaa(), self.get_aaya())
    }
}

#[derive(Debug)]
pub struct Memory {
    pub memory: [u8; 4096],
}

const FONTSET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];

impl Memory {



    pub fn new() -> Memory {
        let mut mem = Memory { memory: [0; 4096] };
        mem.put_slice(0, &FONTSET);
        mem
    }

    pub fn load_program(&mut self, program: &[u8]) {
        self.put_slice(512, program);
    }

    pub fn get_instruction(&self, memory_location: u16) -> u16 {
        u16::from_be_bytes([
            self.memory[memory_location as usize],
            self.memory[(memory_location + 1) as usize],
        ])
    }

    pub fn put_slice(&mut self, start_pos: usize, slice: &[u8]) {
        for (i, byte) in slice.iter().enumerate() {
            self.memory[start_pos + i] = *byte;
        }
    }
}

pub fn draw_screen(screen: &[[bool; SCREEN_WIDTH]; SCREEN_HEIGHT]) {
    const ROWS: usize = SCREEN_HEIGHT;
    const COLS: usize = SCREEN_WIDTH;
    for (row_i, row) in screen.iter().enumerate() {
        for (col_i, &val) in row.iter().enumerate() {
            let start_pos_x = (col_i as f32 / COLS as f32) * screen_width();
            let start_pos_y = (row_i as f32 / ROWS as f32) * screen_height();
            let pixel_size_x = screen_width() / COLS as f32;
            let pixel_size_y = screen_height() / ROWS as f32;

            if val {
                draw_rectangle(start_pos_x, start_pos_y, pixel_size_x, pixel_size_y, WHITE);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod system {
        use super::*;
    }

    mod instructions {
        use super::*;

        fn make(program: &[u8]) -> CPU {
            let mut state = CPU::new();
            state.memory.load_program(program);
            state
        }

        fn make_and_tick(program: &[u8]) -> CPU {
            let mut state = make(program);
            state.fetch_and_run();
            state
        }

        #[test]
        fn test_jump_instruction_sets_pc_to_value() {
            let state = make_and_tick(&[0x13, 0xf6]);
            assert_eq!(state.program_counter, 0x3f6);
        }

        #[test]
        fn test_return_from_a_subroutine() {
            let mut state = make(&[0x00, 0xee]);
            state.stack_pointer = 3;
            state.stack[2] = 0x0432;
            state.fetch_and_run();
            assert_eq!(state.program_counter, 0x432);
            assert_eq!(state.stack_pointer, 2);
        }

        #[test]
        fn test_clear_screen_clears_screen() {
            let mut state = make(&[0x00, 0xE0]);
            state
                .screen
                .iter_mut()
                .for_each(|row| row.iter_mut().for_each(|x| *x = true));
            state.fetch_and_run();
            for row in state.screen.iter() {
                for val in row.iter() {
                    assert_eq!(val, &false);
                }
            }
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_call_subroutine() {
            let mut state = make(&[0x21, 0x23]);
            state.stack_pointer = 2;
            state.fetch_and_run();
            assert_eq!(state.stack_pointer, 3);
            assert_eq!(state.stack[2], 0x200); // pc starts at 0x200
            assert_eq!(state.program_counter, 0x123);
        }

        mod skip_reg_equal_to_const {
            use super::*;

            #[test]
            fn test_skips_when_equal() {
                let mut state = make(&[0x3c, 0xbc]);
                state.registers[0xc] = 0xbc;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instructions
            }

            #[test]
            fn test_does_not_skip_when_not_equal() {
                let mut state = make(&[0x34, 0xb5]);
                state.registers[0x4] = 0x12;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instructions
            }
        }

        mod skip_reg_not_equal_to_const {
            use super::*;

            #[test]
            fn test_skips_when_not_equal() {
                let mut state = make(&[0x44, 0xb5]);
                state.registers[0x4] = 0x12;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instructions
            }

            #[test]
            fn test_does_not_skip_when_equal() {
                let mut state = make(&[0x4c, 0xbc]);
                state.registers[0xc] = 0xbc;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instructions
            }
        }

        mod skip_if_reg_equals_reg {
            use super::*;

            #[test]
            fn test_skips_when_equal() {
                let mut state = make(&[0x5c, 0xb0]);
                state.registers[0xc] = 0x12;
                state.registers[0xb] = 0x12;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instruction
            }

            #[test]
            fn test_does_not_skip_when_not_equal() {
                let mut state = make(&[0x5c, 0xb0]);
                state.registers[0xc] = 0x12;
                state.registers[0xb] = 0x13;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        #[test]
        fn test_set_reg_to_constant() {
            let state = make_and_tick(&[0x6f, 0x8e]);
            assert_eq!(state.registers[0xf as usize], 0x8e);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_add_constant_to_reg() {
            let mut state = make(&[0x70, 0x45]);
            state.registers[0] = 0x6;
            state.fetch_and_run();
            assert_eq!(state.registers[0], 0x6 + 0x45);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_set_reg_to_reg() {
            let mut state = make(&[0x81, 0xb0]);
            state.registers[0xb] = 0x6;
            state.fetch_and_run();
            assert_eq!(state.registers[0x1], 0x6);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_set_reg_to_reg_or_reg() {
            let mut state = make(&[0x81, 0xb1]);
            state.registers[0x1] = 0x1;
            state.registers[0xb] = 0x4;
            state.fetch_and_run();
            assert_eq!(state.registers[0x1], 0x5);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_set_reg_to_reg_and_reg() {
            let mut state = make(&[0x81, 0xb2]);
            state.registers[0x1] = 0x7;
            state.registers[0xb] = 0xc;
            state.fetch_and_run();
            assert_eq!(state.registers[0x1], 0x4);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_set_reg_to_reg_xor_reg() {
            let mut state = make(&[0x81, 0xb3]);
            state.registers[0x1] = 0x7;
            state.registers[0xb] = 0xc;
            state.fetch_and_run();
            assert_eq!(state.registers[0x1], 0xb);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        mod test_set_reg_to_reg_plus_reg {
            use super::*;

            #[test]
            fn without_carry() {
                let mut state = make(&[0x81, 0xb4]);
                state.registers[0x1] = 0x2;
                state.registers[0xb] = 0x3;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x5);
                assert_eq!(state.registers[0xf], 0x0); // register VF should indicate no carry
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn with_carry() {
                let mut state = make(&[0x81, 0xb4]);
                state.registers[0x1] = 0xfd;
                state.registers[0xb] = 0xde;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0xdb);
                assert_eq!(state.registers[0xf], 0x1); // register VF should indicate carry occured
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod test_set_reg_to_reg_minus_reg {
            use super::*;

            #[test]
            fn without_borrow() {
                let mut state = make(&[0x81, 0xb5]);
                state.registers[0x1] = 0xcc;
                state.registers[0xb] = 0x56;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x76);
                assert_eq!(state.registers[0xf], 0x1); // register VF should indicate no borrow (1 for some reason)
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn with_borrow() {
                let mut state = make(&[0x81, 0xb5]);
                state.registers[0x1] = 0x56;
                state.registers[0xb] = 0xcc;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x8a);
                assert_eq!(state.registers[0xf], 0x0); // register VF should indicate borrow occured (0 for some reason)
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod test_reg_to_reg_shr_1 {
            use super::*;

            #[test]
            fn without_underflow() {
                let mut state = make(&[0x81, 0x06]);
                state.registers[0x1] = 0x4;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x2);
                assert_eq!(state.registers[0xf], 0x0); // register VF should indicate no underflow
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn with_underflow() {
                let mut state = make(&[0x81, 0x06]);
                state.registers[0x1] = 0x7;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x3);
                assert_eq!(state.registers[0xf], 0x1); // register VF should indicate underflow
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod test_set_reg_to_reg_subn_reg {
            use super::*;

            #[test]
            fn without_borrow() {
                let mut state = make(&[0x81, 0xb7]);
                state.registers[0x1] = 0x56;
                state.registers[0xb] = 0xcc;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x76);
                assert_eq!(state.registers[0xf], 0x1); // register VF should indicate no borrow (1 for some reason)
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn with_borrow() {
                let mut state = make(&[0x81, 0xb7]);
                state.registers[0x1] = 0xcc;
                state.registers[0xb] = 0x56;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x8a);
                assert_eq!(state.registers[0xf], 0x0); // register VF should indicate borrow occured (0 for some reason)
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod test_reg_to_reg_shl_1 {
            use super::*;

            #[test]
            fn without_underflow() {
                let mut state = make(&[0x81, 0x0e]);
                state.registers[0x1] = 0x4b;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x96);
                assert_eq!(state.registers[0xf], 0x0); // register VF should indicate no underflow
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn with_underflow() {
                let mut state = make(&[0x81, 0x0e]);
                state.registers[0x1] = 0xcb;
                state.fetch_and_run();
                assert_eq!(state.registers[0x1], 0x96);
                assert_eq!(state.registers[0xf], 0x1); // register VF should indicate underflow
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod skip_next_instruction_if_reg_not_equal_to_reg {
            use super::*;

            #[test]
            fn equal_so_no_skip() {
                let mut state = make(&[0x91, 0xb0]);
                state.registers[0x1] = 0x56;
                state.registers[0xb] = 0x56;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn not_equal_so_no_skip() {
                let mut state = make(&[0x91, 0xb0]);
                state.registers[0x1] = 0x57;
                state.registers[0xb] = 0x56;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instructions
            }
        }

        #[test]
        fn test_set_address_register_to_constant() {
            let state = make_and_tick(&[0xaf, 0x8e]);
            assert_eq!(state.address_register, 0xf8e);
            assert_eq!(state.program_counter, 0x202); // incremented one instruction
        }

        #[test]
        fn test_jump_to_location_plus_reg_0() {
            let mut state = make(&[0xbf, 0x8e]);
            state.registers[0x0] = 0x57;
            state.fetch_and_run();
            assert_eq!(state.program_counter, 0xf8e + 0x57);
        }

        #[test]
        fn test_set_reg_to_random_and_constant() {
            let mut state = make_and_tick(&[0xc1, 0x8e, 0xc1, 0x53]);
            // rng is deterministic so we know first two numbers are 213 and 148 using rng.next_u32() as u8
            assert_eq!(state.registers[1], 213_u8 & 0x8e_u8);
            state.fetch_and_run();
            assert_eq!(state.registers[1], 148_u8 & 0x53_u8);
            assert_eq!(state.program_counter, 0x204); // incremented two instructions (since tick called twice)
        }

        mod test_display_sprite {
            use super::*;

            const EXAMPLE_SPRITE: [u8; 4] = [
                0x3C, // 00111100     ****
                0xC3, // 11000011   **    **
                0xFF, // 11111111   ********
                0xE0, // 11100000   ***
            ];

            #[test]
            fn test_displays_single_byte_sprite_at_x0_y0() {
                let mut state = make(&[0xd3, 0x51]);
                state.address_register = 1;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.fetch_and_run();
                assert_eq!(
                    state.screen[0][0..8],
                    [true, true, false, false, false, false, true, true]
                );
            }

            #[test]
            fn test_displays_single_byte_sprite_at_x7_y9() {
                let mut state = make(&[0xd3, 0x51]);
                state.registers[0x3] = 7;
                state.registers[0x5] = 9;
                state.address_register = 1;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.fetch_and_run();
                assert_eq!(
                    state.screen[9][7..7 + 8],
                    [true, true, false, false, false, false, true, true]
                );
            }

            #[test]
            fn test_displays_multi_line_sprite_sprite_at_x7_y9() {
                let mut state = make(&[0xd3, 0x53]);
                state.registers[0x3] = 7;
                state.registers[0x5] = 9;
                state.address_register = 0;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.fetch_and_run();
                assert_eq!(
                    state.screen[9][7..7 + 8],
                    [false, false, true, true, true, true, false, false]
                );
                assert_eq!(
                    state.screen[10][7..7 + 8],
                    [true, true, false, false, false, false, true, true]
                );
                assert_eq!(
                    state.screen[11][7..7 + 8],
                    [true, true, true, true, true, true, true, true]
                );
            }

            #[test]
            fn test_displays_single_byte_asymmetric_sprite_at_x7_y9() {
                let mut state = make(&[0xd3, 0x51]);
                state.registers[0x3] = 7;
                state.registers[0x5] = 9;
                state.address_register = 3;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.fetch_and_run();
                assert_eq!(
                    state.screen[9][7..7 + 8],
                    [true, true, true, false, false, false, false, false]
                );
            }

            #[test]
            fn test_sprites_are_xored() {
                // single byte at 0, 0
                let mut state = make(&[0xd3, 0x51]);
                state.address_register = 1;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.screen[0][1] = true;
                state.fetch_and_run();
                assert_eq!(
                    state.screen[0][0..8],
                    [true, false, false, false, false, false, true, true]
                );
            }

            #[test]
            fn test_erase_flag_cleared_when_no_erase() {
                // single byte at 0, 0
                let mut state = make(&[0xd3, 0x51]);
                state.address_register = 1;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.registers[0xf] = 1; // check it is cleared
                state.fetch_and_run();
                assert_eq!(state.registers[0xf], 0);
            }

            #[test]
            fn test_erase_flag_set_when_erase() {
                // single byte at 0, 0
                let mut state = make(&[0xd3, 0x51]);
                state.address_register = 1;
                state.memory.put_slice(0, &EXAMPLE_SPRITE);
                state.screen[0][1] = true;
                state.fetch_and_run();
                assert_eq!(state.registers[0xf], 1);
            }

            #[test]
            fn test_program_counter_incremented() {
                let state = make_and_tick(&[0xd3, 0x51]);
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }
        }

        mod test_skip_if_key_pressed {
            //  Ex9E - SKP Vx
            use super::*;

            #[test]
            fn test_when_key_not_pressed_no_skip_occurs() {
                let mut state = make(&[0xe3, 0x9e]);
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn test_when_key_pressed_skip_occurs() {
                let mut state = make(&[0xe3, 0x9e]);
                state.registers[0x3] = 5;
                state.keys[5] = true;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instructions
            }
        }

        mod test_skip_if_key_not_pressed {
            //  ExA1 - SKNP Vx
            use super::*;

            #[test]
            fn test_when_key_pressed_no_skip_occurs() {
                let mut state = make(&[0xe3, 0xa1]);
                state.registers[0x3] = 5;
                state.keys[5] = true;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
            }

            #[test]
            fn test_when_key_not_pressed_skip_occurs() {
                let mut state = make(&[0xe3, 0xa1]);
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x204); // incremented two instructions
            }
        }

        mod test_delay_timer_value_placed_into_register {
            use super::*;
            
            //  Fx07 - LD Vx, DT
            //  Set Vx = delay timer value.

            #[test]
            fn test_delay_timer_value_placed_into_register() {
                let mut state = make(&[0xf4, 0x07]);
                state.delay_timer = 0x45;
                state.fetch_and_run();
                assert_eq!(state.registers[4], 0x45);
                assert_eq!(state.program_counter, 0x202);
            }

        }

        mod test_wait_for_key_and_store_value_in_reg {
            use super::*;

            // Fx0A - LD Vx, K
            // Wait for a key press, store the value of the key in Vx.
            #[test]
            fn test_all_execution_stops_until_a_key_is_pressed() {
                let mut state = make(&[
                    0xf3, 0x0a, 0x00, 0xe0, // extra instructions to prevent error
                    0x00, 0xe0, 0x00, 0xe0, 0x00, 0xe0, 0x00, 0xe0, 0x00, 0xe0,
                ]);
                for _ in 0..5 {
                    state.fetch_and_run()
                }
                assert_eq!(state.program_counter, 0x200); // not incremented
                state.keys[5] = true;
                state.fetch_and_run();
                assert_eq!(state.program_counter, 0x202); // incremented one instruction
                assert_eq!(state.registers[3], 5);
            }
        }


        mod test_regiser_placed_into_delay_timer {
            use super::*;
            
            //  Fx15 - LD DT, Vx
            //  Set delay timer = Vx.

            #[test]
            fn test_regiser_placed_into_delay_timer() {
                let mut state = make(&[0xf3, 0x15]);
                state.registers[3] = 0x45;
                state.fetch_and_run();
                assert_eq!(state.delay_timer, 0x45);
                assert_eq!(state.program_counter, 0x202);
            }

        }

        #[test]
        fn test_sound_timer_set_to_reg() {
            // Fx18 - LD ST, Vx
            // Set sound timer = Vx.
            let mut state = make(&[0xf3, 0x18]);
            state.registers[3] = 0x45;
            state.fetch_and_run();
            assert_eq!(state.sound_timer, 0x45);
            assert_eq!(state.program_counter, 0x202);
        }

        #[test]
        fn test_add_address_reg_and_reg_storing_in_address_reg() {
            // Fx1E - ADD I, Vx
            // Set I = I + Vx.
            let mut state = make(&[0xf3, 0x1e]);
            state.registers[3] = 0x45;
            state.address_register = 0x21;
            state.fetch_and_run();
            assert_eq!(state.address_register, 0x45 + 0x21);
            assert_eq!(state.program_counter, 0x202);
        }

        mod test_get_sprite_for_digit_in_reg {
            use super::*;

            //  Fx29 - LD F, Vx
            // Set I = location of sprite for digit Vx.
            #[test]
            fn test_gives_correct_memory_location_for_valid_value() {
                let mut state = make(&[0xf4, 0x29]);
                state.registers[4] = 1;
                state.fetch_and_run();
                assert_eq!(state.address_register, 5);
                assert_eq!(state.program_counter, 0x202);
            }
        }

        mod test_store_bcd_representation_of_reg_in_memory {
            use super::*;
            //  Fx33 - LD B, Vx
            // Store BCD representation of Vx in memory locations I, I+1, and I+2.
            #[test]
            fn test_stores_single_digit() {
                let mut state = make(&[0xf4, 0x33]);
                state.registers[4] = 5;
                state.address_register = 600;
                state.memory.put_slice(600, &[1, 2, 3]);
                state.fetch_and_run();
                assert_eq!(state.memory.memory[600], 0);
                assert_eq!(state.memory.memory[601], 0);
                assert_eq!(state.memory.memory[602], 5);
                assert_eq!(state.program_counter, 0x202);
            }

            #[test]
            fn test_stores_two_digits() {
                let mut state = make(&[0xf4, 0x33]);
                state.registers[4] = 95;
                state.address_register = 600;
                state.memory.put_slice(600, &[1, 2, 3]);
                state.fetch_and_run();
                assert_eq!(state.memory.memory[600], 0);
                assert_eq!(state.memory.memory[601], 9);
                assert_eq!(state.memory.memory[602], 5);
                assert_eq!(state.program_counter, 0x202);
            }

            #[test]
            fn test_stores_three_digits() {
                let mut state = make(&[0xf4, 0x33]);
                state.registers[4] = 254;
                state.address_register = 600;
                state.memory.put_slice(600, &[1, 2, 3]);
                state.fetch_and_run();
                assert_eq!(state.memory.memory[600], 2);
                assert_eq!(state.memory.memory[601], 5);
                assert_eq!(state.memory.memory[602], 4);
                assert_eq!(state.program_counter, 0x202);
            }
        }

        #[test]
        fn test_store_regs_v0_to_vx_in_memory() {
            // Fx55 - LD [I], Vx
            // Store registers V0 through Vx in memory starting at location I.
            let mut state = make(&[0xf3, 0x55]);
            state.address_register = 680;
            state.registers[0] = 3;
            state.registers[1] = 8;
            state.registers[2] = 7;
            state.registers[3] = 54;
            state.registers[4] = 55; // should not be copied
            state.fetch_and_run();
            assert_eq!(state.memory.memory[680..=683], [3, 8, 7, 54]);
            assert_eq!(state.memory.memory[684], 0); // check V4 is not copied
            assert_eq!(state.program_counter, 0x202);
        }

        #[test]
        fn test_read_memory_into_regs_v0_to_vx() {
            // Fx65 - LD Vx, [I]
            // Read registers V0 through Vx from memory starting at location I.
            let mut state = make(&[0xf3, 0x65]);
            state.address_register = 680;
            state.memory.put_slice(680, &[2, 5, 32, 21, 45]);
            state.fetch_and_run();
            assert_eq!(state.registers[0], 2);
            assert_eq!(state.registers[1], 5);
            assert_eq!(state.registers[2], 32);
            assert_eq!(state.registers[3], 21);
            assert_eq!(state.registers[4], 0); // should not have been copied
            assert_eq!(state.program_counter, 0x202);
        }

    }

    mod test_memory {
        use super::*;

        #[test]
        fn test_loads_program_into_memory_and_can_get_instruction() {
            let mut memory = Memory::new();
            memory.load_program(&[0x0f, 0x43, 0xa2, 0xf0, 0x61, 0x23]);
            assert_eq!(memory.get_instruction(0x200), 0x0f43);
            assert_eq!(memory.get_instruction(0x202), 0xa2f0); // incremented one instruction
        }

        #[test]
        fn test_put_slice_puts_slice_into_memory() {
            let mut memory = Memory::new();
            memory.put_slice(10, &[0x01, 0xf3, 0x45]);
            assert_eq!(memory.memory[10..13], [0x01, 0xf3, 0x45]);
        }

        #[test]
        fn test_pre_loaded_with_fontset() {
            let memory = Memory::new();
            assert_eq!(memory.memory[5..=9], [0x20, 0x60, 0x20, 0x20, 0x70])
        }
    }
}
