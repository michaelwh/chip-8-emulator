use macroquad::prelude::*;

use chip_8_emulator::{CPU, SCREEN_WIDTH, SCREEN_HEIGHT};

fn draw_screen(screen: &[[bool; SCREEN_WIDTH]; SCREEN_HEIGHT]) {
    const ROWS: usize = 32;
    const COLS: usize = 64;
    for (row_i, row) in screen.iter().enumerate() {
        for (col_i, &val) in row.iter().enumerate() {
            let start_pos_x = (col_i as f32 / COLS as f32) * screen_width();
            let start_pos_y = (row_i as f32 / ROWS as f32) * screen_height();
            let pixel_size_x = screen_width() / COLS as f32;
            let pixel_size_y = screen_height() / ROWS as f32;

            if val {
                draw_rectangle(start_pos_x, start_pos_y, pixel_size_x, pixel_size_y, GREEN);
            }
        }
    }
}

#[macroquad::main("DrawScreen")]
async fn main() {
    let mut state = CPU::new();
    state.memory.load_program(&[0xd3, 0x53]);

    const EXAMPLE_SPRITE: [u8; 3] = [
        0x3C, // 00111100     ****
        0xC3, // 11000011   **    **
        0xFF, // 11111111   ********
    ];

    state.registers[0x3] = 7;
    state.registers[0x5] = 9;
    state.address_register = 0;
    state.memory.put_slice(0, &EXAMPLE_SPRITE);
    state.fetch_and_run();

    // state.screen[0][0] = true;
    // state.screen[10][10] = true;
    // state.screen[11][10] = true;
    // state.screen[11][11] = true;
    
    // state.screen[31][63] = true;

    loop {
        clear_background(BLACK);
        draw_screen(&state.screen);
        next_frame().await
    }
}